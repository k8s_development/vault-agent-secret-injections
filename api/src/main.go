package main

// import (
// 	"fmt"
// 	"net/http"
//   )

// import (
// 	"github.com/gin-gonic/gin"
// )

// func mainRouter() *gin.Engine {
// 	router := gin.Default()
// 	router.GET("/", getPodInfo)
// 	return router
// }

// func main() {
// 	router := mainRouter()
// 	router.Run("0.0.0.0:8080")
// }

// func main() {
// 	mux := http.NewServeMux()
// 	mux.HandleFunc("GET /path/", func(w http.ResponseWriter, r *http.Request) {
// 	  fmt.Fprint(w, "got path\n")
// 	})

// 	mux.HandleFunc("/task/{id}/", func(w http.ResponseWriter, r *http.Request) {
// 	  id := r.PathValue("id")
// 	  fmt.Fprintf(w, "handling task with id=%v\n", id)
// 	})

// 	http.ListenAndServe("localhost:8090", mux)
//   }

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", HelloServer)
	http.ListenAndServe("0.0.0.0:8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {

	app_env_secret := os.Getenv("APP_SECRET")
	fmt.Fprintf(w, "Hello, %s %s!", "Hello",app_env_secret )
}
